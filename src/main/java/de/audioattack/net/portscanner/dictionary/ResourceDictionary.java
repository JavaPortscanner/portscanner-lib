// Copyright 2016 Marc Nause <marc.nause@gmx.de>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package de.audioattack.net.portscanner.dictionary;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Port dictionary which fetches data from resource file.
 * 
 * Format: <br />
 * portnumber\tprotocol\tdescription <br />
 * ( description is optional)
 * 
 * @since 1.0.0
 */
public class ResourceDictionary extends AbstractDictionary {

  private static final Pattern NUMBER_PATTERN = Pattern.compile("\\d+");
  private static final Pattern SPLIT_PATTERN = Pattern.compile("\\t");
  private static final String EMPTY = "";

  private final Map<Integer, List<ProtocolInfo>> myMapping = new HashMap<Integer, List<ProtocolInfo>>();

  /**
   * Constructor.
   * 
   * @param filename
   *          resource file to fetch data from
   */
  protected ResourceDictionary(final String filename) {
    super(true);

    readData(filename);
  }

  private void readData(final String filename) {

    final BufferedReader reader = new BufferedReader(new InputStreamReader(
        Thread.currentThread().getContextClassLoader().getResourceAsStream(filename)));

    /*
     * We don't use try-with-resources here since we want to support Android.
     * Unfortunately Android only supports try-with-resources if minSdkVersion
     * is set to 19 (Android 4.4) or higher.
     */
    try {

      while (reader.ready()) {
        addLine(SPLIT_PATTERN.split(reader.readLine()));
      }
    } catch (IOException e) {
      throw new IllegalArgumentException("Unable to read " + filename, e);
    } finally {
      try {
        reader.close();
      } catch (IOException e) {
        // we don't care
      }
    }

  }

  private void addLine(final String... line) {

    if (line.length > 1 && NUMBER_PATTERN.matcher(line[0]).matches() && line[1] != null
        && !line[1].isEmpty()) {
      final int port = Integer.parseInt(line[0]);

      if (!myMapping.containsKey(port)) {
        myMapping.put(port, new ArrayList<ProtocolInfo>(1));
      }
      myMapping.get(port).add(new ProtocolInfo(line[1], line.length > 2 ? line[2] : EMPTY));
    }

  }

  @Override
  protected Map<Integer, List<ProtocolInfo>> getMapping() {
    return myMapping;
  }

}
