// Copyright 2015 - 2016 Marc Nause <marc.nause@gmx.de>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package de.audioattack.net.portscanner.iterator;

import java.math.BigInteger;
import java.net.Inet4Address;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Iterator;
import java.util.NoSuchElementException;

import de.audioattack.net.portscanner.util.ByteUtil;
import de.audioattack.net.portscanner.util.NetUtil;

/**
 * Iterator for network addresses (IPv4 and IPv6).
 * 
 * @since 1.0.0
 */
public class InetAddressIterator implements Iterator<InetAddress> {

  private final BigInteger myHighestAddressOfBlock;
  private final BigInteger myTotalNumberOfAddresses;
  private BigInteger myNextAddress;
  private int myArraySize;

  /**
   * Constructor.
   * 
   * @param baseAddress
   *          has to contain address which can be resolved to IPv4 address or
   *          IPv6 address
   * @param networkPrefixLength
   *          determines number of hosts to be scanned
   * @param scanWholeBlock
   *          {@code true} to scan whole block which contains baseAddress,
   *          {@code false} to start with baseAddress
   */
  public InetAddressIterator(final InetAddress baseAddress, final int networkPrefixLength,
      final boolean scanWholeBlock) {

    if (baseAddress instanceof Inet4Address) {
      myArraySize = 4;
    } else if (baseAddress instanceof Inet6Address) {
      myArraySize = 16;
    } else {
      throw new IllegalArgumentException(
          "Unknown InetAddress implementation: " + baseAddress.getClass().getName());
    }

    myNextAddress = new BigInteger(baseAddress.getAddress());

    final BigInteger myFirstAddressOfBlock = myNextAddress
        .and(new BigInteger(NetUtil.getNetmask(baseAddress, networkPrefixLength)));

    myHighestAddressOfBlock = myFirstAddressOfBlock
        .or(new BigInteger(ByteUtil.invert(NetUtil.getNetmask(baseAddress, networkPrefixLength))));

    myTotalNumberOfAddresses = myHighestAddressOfBlock.subtract(myFirstAddressOfBlock)
        .add(BigInteger.ONE);

    if (scanWholeBlock) {
      myNextAddress = myFirstAddressOfBlock;
    }
  }

  @Override
  public boolean hasNext() {

    return myNextAddress.compareTo(myHighestAddressOfBlock) <= 0;
  }

  @Override
  public InetAddress next() {

    InetAddress inetAddress;
    try {
      inetAddress = InetAddress
          .getByAddress(ByteUtil.pad(myArraySize, myNextAddress.toByteArray()));
    } catch (UnknownHostException e) {
      inetAddress = null;
    }

    if (hasNext()) {
      myNextAddress = myNextAddress.add(BigInteger.ONE);
    } else {
      throw new NoSuchElementException();
    }

    return inetAddress;
  }

  /**
   * Gets approximate count of addresses which is iterated over.
   * 
   * @return approximate count
   */
  public BigInteger getApproxCount() {
    return myTotalNumberOfAddresses;
  }

  @Override
  public void remove() {
    throw new UnsupportedOperationException();
  }
}
