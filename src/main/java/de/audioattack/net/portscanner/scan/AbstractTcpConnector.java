// Copyright 2016 Marc Nause <marc.nause@gmx.de>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package de.audioattack.net.portscanner.scan;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.UUID;

import de.audioattack.net.portscanner.listener.IScannerStatusListener;
import de.audioattack.net.portscanner.scan.enums.PortResult;
import de.audioattack.net.portscanner.scan.enums.TransportProtocol;

/**
 * Base class for TCP connectors.
 * 
 * @since 1.0.0
 */
abstract class AbstractTcpConnector extends AbstractScanConnector {

  /**
   * Constructor.
   * 
   * @param jobId
   *          ID of scan job
   * @param inetSocketAddress
   *          address of host to scan
   * @param timeout
   *          max. time to wait for socket connection
   * @param statusListenerReference
   *          callback to report result to
   */
  AbstractTcpConnector(final UUID jobId, final InetSocketAddress inetSocketAddress,
      final int timeout, final WeakReference<IScannerStatusListener> statusListenerReference) {
    super(jobId, inetSocketAddress, timeout, statusListenerReference);
  }

  @Override
  protected PortResult connect() {

    PortResult ret;
    try {
      final Socket socket = new Socket();
      socket.connect(getInetSocketAddress(), getTimeout());
      socket.close();
      ret = PortResult.OPEN;
    } catch (final IOException e) {
      ret = PortResult.CLOSED;
    }
    return ret;
  }

  @Override
  protected TransportProtocol getTransportProtocol() {
    return TransportProtocol.TCP;
  }

}
