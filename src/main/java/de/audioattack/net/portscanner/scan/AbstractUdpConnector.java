// Copyright 2016 Marc Nause <marc.nause@gmx.de>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package de.audioattack.net.portscanner.scan;

import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.PortUnreachableException;
import java.net.SocketTimeoutException;
import java.util.UUID;

import de.audioattack.net.portscanner.listener.IScannerStatusListener;
import de.audioattack.net.portscanner.scan.enums.PortResult;
import de.audioattack.net.portscanner.scan.enums.TransportProtocol;

/**
 * Base class for UDP connectors.
 * 
 * @since 1.0.0
 */
abstract class AbstractUdpConnector extends AbstractScanConnector {

  private static final int MAX_TRIES = 3;

  /**
   * Constructor.
   * 
   * @param jobId
   *          ID of scan job
   * @param inetSocketAddress
   *          address of host to scan
   * @param timeout
   *          max. time to wait for socket connection
   * @param statusListenerReference
   *          callback to report result to
   */
  AbstractUdpConnector(final UUID jobId, final InetSocketAddress inetSocketAddress,
      final int timeout, final WeakReference<IScannerStatusListener> statusListenerReference) {
    super(jobId, inetSocketAddress, timeout, statusListenerReference);
  }

  @Override
  protected PortResult connect() {

    final DatagramPacket packet = new DatagramPacket(new byte[128], 128);
    DatagramSocket toSocket = null;

    for (int tries = 0; tries < MAX_TRIES; tries++) {
      try {

        toSocket = new DatagramSocket();
        toSocket.connect(getInetSocketAddress());
        toSocket.setSoTimeout(getTimeout());
        toSocket.send(packet);
        toSocket.receive(packet);

        return PortResult.OPEN;
      } catch (PortUnreachableException e) {
        return PortResult.CLOSED;
      } catch (SocketTimeoutException e) {
        // ignore silently, we'll try again
      } catch (IOException e) {
        return PortResult.CLOSED;
      } finally {
        if (toSocket != null) {
          toSocket.close();
        }
      }
    }

    return PortResult.OPEN_FILTERED;
  }

  @Override
  protected TransportProtocol getTransportProtocol() {
    return TransportProtocol.UDP;
  }

}
