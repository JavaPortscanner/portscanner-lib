// Copyright 2012 - 2015 Marc Nause <marc.nause@gmx.de>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package de.audioattack.net.portscanner.util;

import java.util.Arrays;

/**
 * Contains utility methods for byte handling and manipulation.
 * 
 * @since 1.0.0
 */
public final class ByteUtil {

  /** Private constructor to avoid instantiation of static utility class. */
  private ByteUtil() {
  }

  /**
   * Creates a bit mask with leading bits set. Will throw Exception of number of
   * set bits is too large to fit into bytes.
   *
   * @param bytes
   *          length of bit mask in byte
   * @param numSet
   *          number of leading bits which are set
   * @return bitmask with leading bits set
   */
  public static byte[] createBitMaskLeadingBitsSet(final int bytes, final int numSet) {
    if (numSet > bytes * 8) {
      throw new IllegalArgumentException(numSet + " does not fit into " + bytes + " bytes.");
    }

    final byte[] byteArray = new byte[bytes];
    int left = numSet;

    for (int i = 0; i < byteArray.length; i++) {

      if (left >= 8) {
        byteArray[i] = (byte) 0xFF;
      } else if (left > 0) {
        byteArray[i] = (byte) ((0xFF << 8 - left) & 0xFF);
      } else {
        byteArray[i] = 0;
      }

      left -= 8;

    }

    return byteArray;
  }

  /**
   * Inverts all bits of a byte array.
   * 
   * @param byteArray
   *          array to invert
   * @return array with inverted bits
   */
  public static byte[] invert(final byte[] byteArray) {

    for (int i = 0; i < byteArray.length; i++) {

      byteArray[i] = (byte) (~byteArray[i] & 0xff);
    }

    return byteArray;
  }

  /**
   * Adds given number of 0-bytes to start of array.
   * 
   * @param arraySize
   *          number of bytes in array after using this method, must be larger
   *          than or equal to length of byteArray
   * @param byteArray
   *          array to add 0-bytes to
   * @return array with added 0-bytes
   */
  public static byte[] pad(final int arraySize, final byte[] byteArray) {

    final byte[] newArray = new byte[arraySize];
    Arrays.fill(newArray, (byte) 0);

    for (int i = 0; i < byteArray.length; i++) {
      newArray[newArray.length - byteArray.length + i] = byteArray[i];
    }

    return newArray;
  }

}
