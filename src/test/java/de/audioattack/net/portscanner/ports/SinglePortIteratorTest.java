package de.audioattack.net.portscanner.ports;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import de.audioattack.net.portscanner.iterator.SinglePortIterator;

public class SinglePortIteratorTest {

  @Test
  public void testSize() {

    int[] ports = new int[] { 0, 1, 2 };
    SinglePortIterator singlePortIterator = new SinglePortIterator(ports);

    assertEquals(ports.length, singlePortIterator.size());
  }

  @Test
  public void testContent() {

    int[] ports = new int[] { 0, 1, 2 };
    SinglePortIterator singlePortIterator = new SinglePortIterator(ports);

    for (int i = 0; singlePortIterator.hasNext(); i++) {

      assertEquals(ports[i], singlePortIterator.next().intValue());
    }
  }

}
